package com.security.demo.realm;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Setter
public class LoginFailureHandler implements AuthenticationFailureHandler {
	
	private String defaultTargetUrl;

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
            AuthenticationException ae) throws IOException, ServletException {
        if (log.isDebugEnabled()) {
            log.debug(" login failure exception : {} ", ae.getMessage());
        }
        response.sendRedirect(defaultTargetUrl);
    }
}
