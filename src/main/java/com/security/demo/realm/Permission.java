package com.security.demo.realm;

public interface Permission {

    boolean implies(Permission p);
}
