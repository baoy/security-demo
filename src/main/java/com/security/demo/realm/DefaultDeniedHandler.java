package com.security.demo.realm;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Setter
public class DefaultDeniedHandler implements AccessDeniedHandler {

	private String defaultTargetUrl;

	@Override
	public void handle(HttpServletRequest request, HttpServletResponse response,
			AccessDeniedException ae) throws IOException, ServletException {
		if (log.isDebugEnabled()) {
            log.debug(" login failure exception : {} ", ae.getMessage());
        }
		response.sendRedirect(defaultTargetUrl);
	}
}
