package com.security.demo.service;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.security.demo.dao.RoleDao;
import com.security.demo.model.Role;
import com.security.demo.util.Pager;

@Service("roleService")
public class RoleService {

	@Autowired
	RoleDao roleDao;

	public Pager<Role> findPager(int start, int limit, Object... args) {
		return roleDao.findPager(start, limit, args);
	}

	public void save(Role role) {
		roleDao.mergeRole(role);
	}

	public Set<String> findResourceIds(String roleId) {
		return roleDao.findResourceIds(roleId);
	}
}
