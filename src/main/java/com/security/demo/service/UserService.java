package com.security.demo.service;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.security.demo.dao.UserDao;
import com.security.demo.model.User;
import com.security.demo.util.Pager;

@Service("userService")
public class UserService  {

    @Autowired
    UserDao userDao;

    
    public User findByUsername(String username) {
        return userDao.findByUsername(username);
    }

    
    public Pager<User> findPager(int start, int limit, Object... args) {
        return userDao.findPager(start, limit, args);
    }
    
    
    public Set<String> findPermissions(String username) {
        return userDao.findPermissions(username);
    }

	
	public Set<String> findRoleIds(String userId) {
		return userDao.findRoleIds(userId);
	}

	
	@Transactional
	public boolean save(User user) {
		return userDao.mergeUser(user);
	}
}
