package com.security.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.security.demo.dao.ResourceDao;
import com.security.demo.model.Resource;

@Service("resourceService")
public class ResourceService {

	@Autowired
	ResourceDao resourceDao;

	public List<Resource> findAll() {
		return resourceDao.findAll();
	}

	public String findPermissionByUrl(String url) {
		Resource r = resourceDao.findByUrl(url);
		return r != null ? r.getPermission() : null;
	}
}
