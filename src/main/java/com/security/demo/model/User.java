package com.security.demo.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class User implements Serializable {

	private static final long serialVersionUID = 1771712772217177089L;
	private String id; // 编号
	private String username; // 用户名
	private String password; // 密码
	private String salt; // 加密密码的盐
	private Boolean locked = Boolean.FALSE;

	// 业务字段 后期单独提取一张表
	private String nickname; // 昵称
	private String phone; // 手机
	private Date createtime; // 创建时间
	private short sex; // 性别

	// vo
	List<String> roleIds;

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);  
	}

}
