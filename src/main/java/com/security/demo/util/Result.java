package com.security.demo.util;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class Result {

    Object data;
    String msg;
    Boolean success = false;

    public static Result success(String msg) {
        return new Result(null, msg, true);
    }

    public static Result failure(String msg) {
        return new Result(null, msg, false);
    }

    public static Result writeSuccessData(Object data) {
        return new Result(data, null, true);
    }

    public static Result writeFailureData(Object data) {
        return new Result(data, null, false);
    }
}
