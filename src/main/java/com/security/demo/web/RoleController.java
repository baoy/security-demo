package com.security.demo.web;

import java.util.Set;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.security.demo.model.Role;
import com.security.demo.service.ResourceService;
import com.security.demo.service.RoleService;
import com.security.demo.util.Pager;
import com.security.demo.util.Result;

@Controller
@RequestMapping("/role")
public class RoleController {

	@Autowired
	RoleService roleService;
	@Autowired
	ResourceService resourceService;
	
	@RequestMapping
	public String index() {
		return "role";
	}

	@ResponseBody
	@RequestMapping("/page")
	public Pager<Role> findPager(@RequestParam("start") int start, @RequestParam("limit") int limit) {
		return roleService.findPager(start, limit);
	}

	@ResponseBody
	@RequestMapping("/save")
	public Result save(Role role) {
		roleService.save(role);
		return Result.success("操作成功.");
	}


	@ResponseBody
	@RequestMapping("/resources")
	public Set<String> findRoleResources(String roleId) {
		return roleService.findResourceIds(roleId);
	}

}
