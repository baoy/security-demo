package com.security.demo.web;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.security.demo.model.User;
import com.security.demo.service.UserService;
import com.security.demo.util.Pager;
import com.security.demo.util.Result;

@Controller
@RequestMapping("/user")
public class UserController {

	@Autowired
	UserService userService;

	@RequestMapping
	public String index() {
		return "user";
	}

	@ResponseBody
	@RequestMapping("/page")
	public Pager<User> findPager(@RequestParam("start") int start, @RequestParam("limit") int limit) {
		return userService.findPager(start, limit);
	}

	@ResponseBody
	@RequestMapping("/roles")
	public Set<String> findUserRoles(String userId) {
		return userService.findRoleIds(userId);
	}

	@ResponseBody
	@RequestMapping("/save")
	public Result save(User user) {
		userService.save(user);
		return Result.success("操作成功.");
	}
}
