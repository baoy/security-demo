package com.security.demo.web;



import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class LoginController {

    @RequestMapping("/login")
    public String login() {
        return "login";
    }

    @RequestMapping("/noPermission")
    public String noPermission() {
    	return "noPermission";
    }

    @RequestMapping({"/", "/index"})
    public String index() {
        return "home";
    }

}
